from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^clear$', views.clear, name='clear'),
    url(r'^api/hello$', views.hello, name='api_hello'),
    url(r'^api/reply$', views.reply, name='api_reply')
]